package main;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.Iterator;
import java.util.LinkedList;

public class ChatServer {
	
	private final int port;
	private LinkedList<ClientThread>list=new LinkedList<>();
	private boolean secure;
	private String passwort;
	
	public static void main(String[] args) {
		int port=12345;
		String passwort=null;
		for (int i = 0; i < args.length; i++) {
			switch (args[i]) {
			case "-p":
				try {
					port=Integer.parseInt(args[++i]);
				}
				catch (NumberFormatException e) {
					System.err.println("Port muss eine Ganzzahl sein.");
					e.printStackTrace();
				}
				catch (ArrayIndexOutOfBoundsException e) {
					System.err.println("Port nicht angegeben.");
					e.printStackTrace();
				}
				break;
			case "-s":
				try {
					passwort=args[++i];
				}
				catch (ArrayIndexOutOfBoundsException e) {
					System.err.println("Passwort muss zur Verschl�sselung festgelegt werden.");
					e.printStackTrace();
				}
				break;
			default:
				System.err.println("Unbekannter Parameter: "+args[i]);
				break;
			}
		}
		ChatServer cs=new ChatServer(port,passwort);
		cs.start();
	}
	
	public ChatServer(int port) {
		this(port,null);
	}

	public ChatServer(int port,String password) {
		this.port=port;
		this.passwort=password;
		secure=passwort!=null;
	}
	
	public boolean isSecure() {
		return secure;
	}
	
	String getPasswort() {
		return passwort;
	}
	
	/**
	 * Startet den Server
	 */
	public void start() {
		Thread serverThread=new Thread(()->{
			ServerSocket server=null;
			try {
				server=new ServerSocket(port);
				System.out.println("Server l�uft auf Port "+port);
				System.out.println("Verbindung ist "+(secure?"gesichert":"nicht gesichert"));
				while(true) {// f�r jeden Client einen Thread
					ClientThreadFactory.createClientThread(server.accept(), this).start();
					//new ClientThread(server.accept(), this).start();
					System.out.println("neue Verbindung");
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
			finally {
				if(server!=null)
					try {
						server.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
			}

		});
		serverThread.start();
	}

	/**
	 * Sendet eine Nachricht an alle verbundenen Clients
	 * 
	 * @param	name	Der Autor der Nachricht
	 * @param	msg	Die Nachricht selbst
	 */
	public void message(String name, String msg) {
		System.out.println(name+":\t"+msg);
		for (ClientThread clientThread : list) {
			clientThread.newMessage(name, msg);
		}
	}

	/**
	 * Registriert einen neuen Client am Server
	 * 
	 * @param client Der zum Client zugeh�rige Thread
	 */
	public void register(ClientThread client) {
		synchronized(list) {
			System.out.println("neuer Client: "+client.getDisplayName());
			list.add(client);
			updateClients();
		}
	}
	
	/**
	 * Nimmt die Liste der registrierten Client und sendet die Namen der registrierten Clients an alle Clients
	 */
	protected void updateClients() {
		String[] namen=new String[list.size()];
		Iterator<ClientThread> i=list.iterator();
		for (int j = 0; j < namen.length; j++) {
			namen[j]=i.next().getDisplayName();
		}
		for (ClientThread cThread : list) {
			cThread.updateUsers(namen);
		}
	}
	
	/**
	 * Entfernet einen bereits registreirten Client
	 * 
	 * @param client
	 */
	public void unregister(ClientThread client) {
		synchronized(list) {
			System.out.println("Client entfernt: "+client.getDisplayName());
			list.remove(client);
			updateClients();
		}
	}
}
