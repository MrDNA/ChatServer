package main;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.Socket;
import java.security.AlgorithmParameters;
import java.security.GeneralSecurityException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

public class SecureClientThread extends ClientThread {
	
	private static final int keyDerivationIterationCount=20000;
	private static byte[] salt;
	private static SecretKey secretKey;

	public SecureClientThread(Socket client, ChatServer server) throws IOException {
		super(client, server);
		if(salt==null) {
				SecureRandom srand=new SecureRandom();
				salt=new byte[8];
				System.out.println("Generiere Salt...");
				srand.nextBytes(salt);
				System.out.println("salt: "+new String(salt));
		}
		if(secretKey==null) {
			try {
				SecretKeyFactory factory=SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256");
				System.out.println("Generiere Schl�ssel...");
				KeySpec spec=new PBEKeySpec(server.getPasswort().toCharArray(), salt, keyDerivationIterationCount, 256);
				SecretKey tmp = factory.generateSecret(spec);
				secretKey = new SecretKeySpec(tmp.getEncoded(), "AES");
				System.out.println("erfolgreich");

			} catch (NoSuchAlgorithmException e) {
				e.printStackTrace();
			} catch (InvalidKeySpecException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	protected void init() {
		try {
			byte[] puffer=new byte[BUFFER_SIZE];//Das erste was vom Client gesendet wird, ist der Name des Clients. 
			int read=in.read(puffer);
			try {
				name=new String(puffer,0,read,"UTF-8");		//der Name ist max. 256 Zeichen lang.
			}
			catch (UnsupportedEncodingException e) {
				e.printStackTrace();
				name=new String(puffer, 0, read);
			}
		} 
		catch (IOException e) {
			e.printStackTrace();
			try {//bei Fehlern wird die Verbindung wiedergeschlossen
				in.close();
				out.close();
				socket.close();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			connected=false;
		}
		System.out.println("sende ini");
		byte[]ini=new byte[4+salt.length];
		ini[0]='I';ini[1]='N';ini[2]='I';ini[3]='\0';
		int j=4;
		for (int i = 0; i < salt.length; i++) {
			ini[j++]=salt[i];
		}
		super.write(ini);
		server.register(this);//mit dem namen wird am Server registriert.
	}
	
	@Override
	protected synchronized void write(byte[] data) {
		try {
			System.out.println("encrypting");
			Cipher cipher=Cipher.getInstance("AES/CBC/PKCS5Padding");
			cipher.init(Cipher.ENCRYPT_MODE, secretKey);
			AlgorithmParameters params = cipher.getParameters();
			byte[] iv = params.getParameterSpec(IvParameterSpec.class).getIV();
			System.out.println("iv length: "+iv.length);
			byte[] ciphertext = cipher.doFinal(data);
			System.out.println("encrypted");
			byte[]msg=new byte[4+iv.length+ciphertext.length];
			msg[0]='S';msg[1]='E';msg[2]='C';msg[3]='\0';
			int i=4;
			for (int j = 0; j < iv.length; j++) {
				msg[i]=iv[j];
				i++;
			}
			for (int j = 0; j < ciphertext.length; j++) {
				msg[i]=ciphertext[j];
				i++;
			}
			System.out.println("cipherlength: "+ciphertext.length);
			System.out.println("sende "+new String(msg));
			super.write(msg);
			
			
		} catch (GeneralSecurityException e) {
			e.printStackTrace();
			super.write(data);
		}
	}
	
	@Override
	protected void processMessage(byte[] msg) {
		System.out.println("empfange "+msg);
		String str;
		try {
			str=new String(msg,"UTF-8");
		} catch (UnsupportedEncodingException e1) {
			e1.printStackTrace();
			str=new String(msg);
		}
		if(!str.startsWith("SEC\0")) {
			super.processMessage(msg);
			return;
		}
		
		byte data[]=new byte[msg.length-4];
		for (int i = 0; i < data.length; i++) {
			data[i]=msg[i+4];
		}
		try {
			System.out.println("decrypting");
			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
			int ivlen=cipher.getParameters().getParameterSpec(IvParameterSpec.class).getIV().length;
			System.out.println("iv length: "+ivlen);
			byte[]iv =new byte[ivlen];
			int cipherlen=data.length-ivlen;
			System.out.println("cipherlen: "+cipherlen);
			byte[]ciphertext=new byte[cipherlen];
			for (int i = 0; i < iv.length; i++) {
				iv[i]=data[i];
			}
			int j=ivlen;
			for (int i = 0; i < ciphertext.length; i++) {
				if(j < data.length) {
					ciphertext[i]=data[j];
					j++;
				}
				else
					ciphertext[i]=0;
			
			}
			cipher.init(Cipher.DECRYPT_MODE, secretKey, new IvParameterSpec(iv));
			msg=(cipher.doFinal(ciphertext));
			
		} catch (GeneralSecurityException e) {
			e.printStackTrace();			
		}
		super.processMessage(msg);
	}
	
}
