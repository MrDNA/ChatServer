package main;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.Socket;

public class ClientThread extends Thread {

	protected Socket socket;
	protected ChatServer server;
	protected InputStream in;
	protected OutputStream out;
	protected String name;
	protected static final int BUFFER_SIZE=256;
	protected boolean connected=true;
	
	public ClientThread(Socket client, ChatServer server) throws IOException {
		socket=client;
		this.server=server;
		in=socket.getInputStream();
		out=socket.getOutputStream();
	}
	
	/**
	 * Gibt den Namen des Clients zur�ck, mit dem er sich angemeldet hat.
	 * 
	 * @return Den Namen des Clients
	 */
	public String getDisplayName() {
		return name;
	}
	
	/**
	 * Sendet eine Nachricht an den Client
	 * 
	 * @param name 	Der Autor der Nachricht
	 * @param msg	Die Nachricht selbst
	 */
	public void newMessage(String name, String msg) {
		String data="MSG\0"+name+"\0"+msg;		//\0 als Separator
		try {
			write(data.getBytes("UTF-8"));
		} catch (UnsupportedEncodingException e) {
			write(data.getBytes());
			e.printStackTrace();
		}
	}
	
	/**
	 * Schreibt Daten in den Ausgabestream.
	 * Schlie�t das Socket und meldet den Client vom Server ab, falls eine IOException auftritt
	 * 
	 * @param data
	 */
	protected synchronized void write(byte[] data) {
		try {
			out.write(data);
		} catch (IOException e) {
			e.printStackTrace();
			try {
				in.close();
				out.close();
				socket.close();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			server.unregister(this);
			connected=false;
		}
	}
	
	/**
	 * Wird vor der Dauerschleife einmal ausgef�hrt, um den Client am Server zu registrieren.
	 * 
	 */
	protected void init() {
		try {
			byte[] puffer=new byte[BUFFER_SIZE];//Das erste was vom Client gesendet wird, ist der Name des Clients. 
			int read=in.read(puffer);
			try {
				name=new String(puffer,0,read,"UTF-8");		//der Name ist max. 256 Zeichen lang.
			}
			catch (UnsupportedEncodingException e) {
				e.printStackTrace();
				name=new String(puffer,0,read);
			}
			server.register(this);//mit dem wird am Server registriert.
		} catch (IOException e) {
			e.printStackTrace();
			try {//bei Fehlern wird die Verbindung wiedergeschlossen
				in.close();
				out.close();
				socket.close();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			connected=false;
		}
	}
	
	@Override
	public void run() {
		init();
		while(connected) {//in dauerschleife Nachrichten lesen
			try {
				int read;
				Puffer puffer=new Puffer(BUFFER_SIZE);
				byte[] bytes=new byte[BUFFER_SIZE];
				do {
					read=in.read(bytes);
					if(read==-1) {//die Verbindung wurde geschlossen
						server.unregister(this);
						return;
					}
					puffer.add(bytes,read);
				}while(read>=BUFFER_SIZE);
				processMessage(puffer.getData());
				
			} catch (IOException e) {//bei Fehlern Verbindung schlie�en
				e.printStackTrace();
				try {
					in.close();
					out.close();
					socket.close();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
				server.unregister(this);
				connected=false;
			}
		}
	}
	
	/**
	 * Verabeitet die erhaltene Nachricht
	 * 
	 * @param msg
	 */
	protected void processMessage(byte[] msg) {
		String str;
		try {
			str=new String(msg,"UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			str=new String(msg);
		}

		server.message(name,str);
	}
	
	/**
	 * Sendet dem Client eine Liste mit Namen der CLients. 
	 * 
	 * @param namen Die Namen der Clients
	 */ 
	public void updateUsers(String[] namen) {
		String data="UPD";
		for (String string : namen) {
			data+="\0"+string;
		}
		try {
			write(data.getBytes("UTF-8"));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			write(data.getBytes());
		}
	}
	
}
