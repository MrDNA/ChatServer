package main;

import java.io.IOException;
import java.net.Socket;

public class ClientThreadFactory {

	/**
	 * Erzeugt einen passenden Thread f�r den Server
	 * 
	 * @param socket der Socket zum Client
	 * @param server der Server
	 * @return einen Client Thread
	 * @throws IOException falls was schlief l�uft
	 */
	public static ClientThread createClientThread(Socket socket,ChatServer server) throws IOException {
		ClientThread ct;
		if(server.isSecure()) {
			System.out.println("secthread");
			ct=new SecureClientThread(socket, server);
		}
		else {
			ct=new ClientThread(socket, server);
		}
		return ct;
	}

}
